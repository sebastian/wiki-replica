# Styleguide

[The Tor Styleguide](https://styleguide.torproject.org) is the living visual identity of Tor's software projects and an integral part of our user experience. The Styleguide is aimed at web applications, but i could be used in any projects that can use css.

The Tor Styleguide is based on Bootstrap, an open-source toolkit for developing with HTML, CSS, and JS. To use the Tor styleguide, you can download our css style and import it in your project. Please refer to the Styleguide [getting started](https://styleguide.torproject.org/getting-started/) page for more information.

Tor Styleguide is based on [Lektor](https://www.getlektor.com/). You can also check [Styleguide repository](https://gitlab.torproject.org/tpo/web/styleguide/).

The Styleguide is hosted at several computers for redundancy, and these computers are together called "the www rotation". Please check the [static sites](doc/static-sites/) help page for more info.
