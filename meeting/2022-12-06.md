# Roll call: who's there and emergencies

the usual fires. anarcat, kez, lavamind present.

# Dashboard review

Normal per-user check-in:
 
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=anarcat
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=kez
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=lavamind

General dashboards:

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

# 2023 roadmap discussion

Discuss and adopt TPA-RFC-42:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-42-roadmap-2023

https://gitlab.torproject.org/tpo/tpa/team/-/issues/40924

Revised proposal:

 * do the bookworm upgrades, this includes:
   * puppet server 7
   * puppet agent 7
   * plan would be:
     * Q1-Q2: deploy new machines with bookworm
     * Q1-Q4: upgrade existing machines to bookworm
 * email services improvements (TPA-RFC-44 2nd generation)
 * upgrade Schleuder and Mailman
 * self-hosting Discourse?
 * complete the cymru migration (e.g. execute TPA-RFC-40)
 * retire gitolite/gitweb (e.g. execute TPA-RFC-36)
 * retire SVN (e.g. execute TPA-RFC-11)
 * monitoring system overhaul (TPA-RFC-33)
 * deploy a Puppet CI

Meeting on wednesday for the web stuff.

Proposal adopted. Worries about our capacity at hosting email, some of
the concerns are shared inside the team, but there doesn't seem to be
many other options for the scale we're working at.

# Holidays confirmation

Confirmed people's dates of availability for the holidays.

# Next meeting

January 9th.

# Metrics of the month

 * hosts in Puppet: 94, LDAP: 94, Prometheus exporters: 163
 * number of Apache servers monitored: 31, hits per second: 744
 * number of self-hosted nameservers: 6, mail servers: 9
 * pending upgrades: 0, reboots: 4
 * average load: 0.83, memory available: 4.46 TiB/5.74 TiB, running processes: 745
 * disk free/total: 33.12 TiB/92.27 TiB
 * bytes sent: 404.70 MB/s, received: 230.86 MB/s
 * planned bullseye upgrades completion date: 2022-11-16, AKA
   "suspicious completion time in the past, data may be incomplete"
 * [GitLab tickets][]: 183 tickets including...
   * open: 0
   * icebox: 152
   * backlog: 14
   * next: 9
   * doing: 5
   * needs information: 3
   * (closed: 2954)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at
https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bullseye/

Now also available as the main Grafana dashboard. Head to
<https://grafana.torproject.org/>, change the time period to 30 days,
and wait a while for results to render.

# Number of the month

Three hundred thousand. The number of subscribers to the Tor
newsletter (!).
